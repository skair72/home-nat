# Server setup
* Partial disks and etc - https://medium.com/better-programming/build-your-own-in-home-cloud-storage-part-2-8fe86e9bd5bf
* Remove swap
```shell
ls / # make sure your swapfile is called swapfile
sudo swapoff -v /swapfile
sudo vi /etc/fstab  # remove the line for the swapfile
sudo rm /swapfile
```

# Architecture prereq
## Setup swarm
```shell
sudo su
export USE_HOSTNAME=skair.online
# Set up the server hostname
echo $USE_HOSTNAME > /etc/hostname
hostname -F /etc/hostname
```
```shell
# Install the latest updates
apt-get update
apt-get upgrade -y
```
```shell
exit
# Download Docker
curl -fsSL get.docker.com -o get-docker.sh
# Install Docker using the stable channel (instead of the default "edge")
CHANNEL=stable sh get-docker.sh
# Adding your user to the “docker” group
sudo usermod -aG docker <your-user>

# Remove Docker install script
rm get-docker.sh
```
```shell
docker swarm init
```
## Traefik Proxy with HTTPS
### Preparation
```shell
docker network create --driver=overlay traefik-public
export NODE_ID=$(docker info -f '{{.Swarm.NodeID}}')
docker node update --label-add traefik-public.traefik-public-certificates=true $NODE_ID
```

```shell
export EMAIL=evenko100500@gmail.com
# Variable with the domain you want to use for the Traefik UI
export DOMAIN=traefik.skair.online
export USERNAME=admin
export PASSWORD=changethis
export HASHED_PASSWORD=$(openssl passwd -apr1 $PASSWORD)
```
Using traefik.yml file
```shell
docker stack deploy -c traefik.yml traefik
```
See logs
```shell
docker service logs traefik_traefik
```
### Swarmpit web user interface
```shell
export DOMAIN=swarmpit.skair.online
export NODE_ID=$(docker info -f '{{.Swarm.NodeID}}')
docker node update --label-add swarmpit.db-data=true $NODE_ID
docker node update --label-add swarmpit.influx-data=true $NODE_ID
docker stack deploy -c swarmpit.yml swarmpit
docker stack ps swarmpit
docker service logs swarmpit_app
```
### Swarmprom for real-time monitoring and alerts
```shell
git clone https://github.com/stefanprodan/swarmprom.git
cp swarmprom.yml swarmprom
cd swarmprom
```
```shell
export ADMIN_USER=admin
export ADMIN_PASSWORD=changethis
export HASHED_PASSWORD=$(openssl passwd -apr1 $ADMIN_PASSWORD)
export DOMAIN=skair.online
```
```shell
docker stack deploy -c swarmprom.yml swarmprom
cd ..
```
### Portainer web user interface
```shell
export DOMAIN=portainer.skair.online
export NODE_ID=$(docker info -f '{{.Swarm.NodeID}}')
docker node update --label-add portainer.portainer-data=true $NODE_ID
```
```shell
docker stack deploy -c portainer.yml portainer
docker stack ps portainer
docker service logs portainer_portainer
```

## Architecture setup